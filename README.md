# References Migration

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation

INTRODUCTION
------------

This module provides views migrate from drupal 7 to drupal 8 or 9. 

 - Download and install the migrate_plus module into your new Drupal 8 site

REQUIREMENTS
------------

 * This module requires migrate_plus module.

INSTALLATION
------------

The installation of this module is like other Drupal modules.

 1. Copy/upload the references_migration module to the modules directory.

 2. Enable the 'references_migration' module and desired sub-modules in 'Extend'.
   (/admin/modules)
