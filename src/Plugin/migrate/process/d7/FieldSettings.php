<?php

namespace Drupal\references_migration\Plugin\migrate\process\d7;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_references_field_settings"
 * )
 */
class FieldSettings extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $value = $row->getSourceProperty('settings');
    switch ($row->getSourceProperty('type')) {
      case 'user_reference':
        $value['target_type'] = 'user';
        break;

      case 'node_reference':
        $value['target_type'] = 'node';
        break;

      default:
        break;
    }

    return $value;
  }

}
