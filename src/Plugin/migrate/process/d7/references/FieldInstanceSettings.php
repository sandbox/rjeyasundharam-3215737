<?php

namespace Drupal\references_migration\Plugin\migrate\process\d7\references;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * @MigrateProcessPlugin(
 *   id = "d7_references_field_instance_settings"
 * )
 */
class FieldInstanceSettings extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    list($instance_settings, $widget_settings, $field_definition) = $value;
    $widget_type = $widget_settings['type'];

    $field_data = unserialize($field_definition['data']);
    $reference_type = $row->getSourceProperty('reference_type');
    if ($reference_type == 'node_reference') {
      $instance_settings['handler'] = 'default:node';

      $instance_settings['handler_settings'] = [
        'sort' => [
          'field' => '_none',
          'direction' => 'ASC',
        ],
        'target_bundles' => [],
      ];
      foreach ($field_data['settings']['referenceable_types'] as $key => $value) {
        if($value)
          $instance_settings['handler_settings']['target_bundles'][$value]=$value;
      }
    }

    if ($reference_type == 'user_reference') {
      $instance_settings['handler'] = 'default:user';

      $instance_settings['handler_settings'] = [
        'include_anonymous' => TRUE,
        'filter' => [
          'type' => '_none',
        ],
        'sort' => [
          'field' => '_none',
          'direction' => 'ASC',
        ],
        'auto_create' => FALSE,
      ];

      if ($row->hasSourceProperty('roles')) {
        $instance_settings['handler_settings']['filter']['type'] = 'role';
        foreach ($row->get('roles') as $role) {
          $instance_settings['handler_settings']['filter']['role'] = [
            $role['name'] => $role['name'],
          ];
        }
      }
    }
    return $instance_settings;
  }

}
