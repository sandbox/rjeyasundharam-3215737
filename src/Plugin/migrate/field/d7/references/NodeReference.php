<?php

namespace Drupal\references_migration\Plugin\migrate\field\d7\references;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\field\ReferenceBase;

// cspell:ignore node_reference

/**
 * MigrateField Plugin for Drupal 6 user reference fields.
 *
 * @MigrateField(
 *   id = "node_reference",
 *   core = {7},
 *   type_map = {
 *     "node_reference" = "entity_reference",
 *   },
 *   source_module = "node_reference",
 *   destination_module = "entity_reference",
 * )
 *
 * @internal
 */
class NodeReference extends ReferenceBase {

  /**
   * The plugin ID for the reference type migration.
   *
   * @var string
   */
  protected $nodeTypeMigration = 'd7_node';

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeMigrationId() {
    return $this->nodeTypeMigration;
  }

  /**
   * {@inheritdoc}
   */
  protected function entityId() {
    return 'nid';
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'target_id' => [
          'plugin' => 'migration_lookup',
          'migration' => 'd7_node',
          'source' => 'nid',
        ],
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'node_reference_default' => 'entity_reference_label',
      'node_reference_link' => 'entity_reference_label',
      'node_reference_plain' => 'entity_reference_label',
      'node_reference_rss_category' => 'entity_reference_label',
      'i18n_node_reference_link' => 'entity_reference_label',
      'entityreference_entity_view' => 'entity_reference_entity_view',
    ];
  }
}
