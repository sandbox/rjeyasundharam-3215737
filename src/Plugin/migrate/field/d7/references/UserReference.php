<?php

namespace Drupal\references_migration\Plugin\migrate\field\d7\references;

use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate_drupal\Plugin\migrate\field\ReferenceBase;

// cspell:ignore user_reference

/**
 * MigrateField Plugin for Drupal 6 user reference fields.
 *
 * @MigrateField(
 *   id = "user_reference",
 *   core = {7},
 *   type_map = {
 *     "user_reference" = "entity_reference",
 *   },
 *   source_module = "user_reference",
 *   destination_module = "entity_reference",
 * )
 *
 * @internal
 */
class UserReference extends ReferenceBase {

  /**
   * The plugin ID for the reference type migration.
   *
   * @var string
   */
  protected $userTypeMigration = 'd7_user';

  /**
   * {@inheritdoc}
   */
  protected function getEntityTypeMigrationId() {
    return $this->userTypeMigration;
  }

  /**
   * {@inheritdoc}
   */
  protected function entityId() {
    return 'uid';
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'target_id' => [
          'plugin' => 'migration_lookup',
          'migration' => 'd7_user',
          'source' => 'uid',
        ],
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }


  /**
   * {@inheritdoc}
   */
  public function getFieldFormatterMap() {
    return [
      'user_reference_default' => 'entity_reference_label',
      'user_reference_link' => 'entity_reference_label',
      'user_reference_plain' => 'entity_reference_label',
      'user_reference_rss_category' => 'entity_reference_label',
      'i18n_user_reference_link' => 'entity_reference_label',
      'entityreference_entity_view' => 'entity_reference_entity_view',
    ];
  }
}
