<?php

namespace Drupal\references_migration\Plugin\migrate\source\d7\references;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 field source from database.
 *
 * If the Drupal 7 Title module is enabled, the fields it provides are not
 * migrated. The values of those fields will be migrated to the base fields they
 * were replacing.
 *
 * For available configuration keys, refer to the parent classes:
 * @see \Drupal\migrate\Plugin\migrate\source\SqlBase
 * @see \Drupal\migrate\Plugin\migrate\source\SourcePluginBase
 *
 * @MigrateSource(
 *   id = "d7_references_field",
 *   source_module = "field_sql_storage"
 * )
 */
class Field extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $types = ['node_reference','user_reference'];
    $query = $this->select('field_config', 'fc')
      ->distinct()
      ->fields('fc')
      ->fields('fci', ['entity_type'])
      ->condition('fc.active', 1)
      ->condition('fc.type', $types, 'IN')
      ->condition('fc.storage_active', 1)
      ->condition('fc.deleted', 0)
      ->condition('fci.deleted', 0);
    $query->join('field_config_instance', 'fci', '[fc].[id] = [fci].[field_id]');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The field ID.'),
      'field_name' => $this->t('The field name.'),
      'type' => $this->t('The field type.'),
      'module' => $this->t('The module that implements the field type.'),
      'active' => $this->t('The field status.'),
      'storage_type' => $this->t('The field storage type.'),
      'storage_module' => $this->t('The module that implements the field storage type.'),
      'storage_active' => $this->t('The field storage status.'),
      'locked' => $this->t('Locked'),
      'data' => $this->t('The field data.'),
      'cardinality' => $this->t('Cardinality'),
      'translatable' => $this->t('Translatable'),
      'deleted' => $this->t('Deleted'),
      'instances' => $this->t('The field instances.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row, $keep = TRUE) {
    
    $data = unserialize($row->getSourceProperty('data'));
    foreach ($data as $key => $value) {
      $row->setSourceProperty($key, $value);
    }
    $type = $row->getSourceProperty('type');
    $row->setSourceProperty('type', 'entity_reference');
    $type = explode('_',$type);
    $row->setSourceProperty('field_type', 'entity_reference');
    $row->setSourceProperty('entity_type', 'node');
    $settings = [];
    if(isset($data['settings'])){
      $settings['target_type']=$type[0];
      $row->setSourceProperty('settings', $settings);
    }
    $instances = $this->select('field_config_instance', 'fci')
      ->fields('fci')
      ->condition('field_name', $row->getSourceProperty('field_name'))
      ->condition('entity_type', $row->getSourceProperty('entity_type'))
      ->execute()
      ->fetchAll();
    $row->setSourceProperty('instances', $instances);

    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'field_name' => [
        'type' => 'string',
        'alias' => 'fc',
      ],
      'entity_type' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
    ];
  }

}
