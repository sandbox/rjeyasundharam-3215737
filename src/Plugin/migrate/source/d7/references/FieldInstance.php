<?php

namespace Drupal\references_migration\Plugin\migrate\source\d7\references;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Drupal 7 field instances source from database.
 *
 * If the Drupal 7 Title module is enabled, the fields it provides are not
 * migrated. The values of those fields will be migrated to the base fields they
 * were replacing.
 *
 * In this example field instances of page content type are retrieved from the
 * source database.
 *
 * For additional configuration keys, refer to the parent classes:
 * @see \Drupal\migrate\Plugin\migrate\source\SqlBase
 * @see \Drupal\migrate\Plugin\migrate\source\SourcePluginBase
 *
 * @MigrateSource(
 *   id = "d7_references_field_instance",
 *   source_module = "field"
 * )
 */
class FieldInstance extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $types = ['node_reference','user_reference'];
    $query = $this->select('field_config_instance', 'fci')
      ->fields('fci')
      ->fields('fc', ['type', 'translatable'])
      ->condition('fc.active', 1)
      ->condition('fc.storage_active', 1)
      ->condition('fc.deleted', 0)
      ->condition('fci.deleted', 0);
    $query->join('field_config', 'fc', '[fci].[field_id] = [fc].[id]');
    $query->condition('fc.type', $types, 'IN');

    // Optionally filter by entity type and bundle.
    if (isset($this->configuration['entity_type'])) {
      $query->condition('fci.entity_type', $this->configuration['entity_type']);

      if (isset($this->configuration['bundle'])) {
        $query->condition('fci.bundle', $this->configuration['bundle']);
      }
    }
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  protected function initializeIterator() {
    $results = $this->prepareQuery()->execute()->fetchAll();

    // Group all instances by their base field.
    $instances = [];
    foreach ($results as $result) {
      $instances[$result['field_id']][] = $result;
    }

    // Add the array of all instances using the same base field to each row.
    $rows = [];
    foreach ($results as $result) {
      $result['instances'] = $instances[$result['field_id']];
      $rows[] = $result;
    }

    return new \ArrayIterator($rows);
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The field instance ID.'),
      'field_id' => $this->t('The field ID.'),
      'field_name' => $this->t('The field name.'),
      'entity_type' => $this->t('The entity type.'),
      'bundle' => $this->t('The entity bundle.'),
      'data' => $this->t('The field instance data.'),
      'deleted' => $this->t('Deleted'),
      'type' => $this->t('The field type'),
      'instances' => $this->t('The field instances.'),
      'field_definition' => $this->t('The field definition.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    foreach (unserialize($row->getSourceProperty('data')) as $key => $value) {
      $row->setSourceProperty($key, $value);
    }

    $field_definition = $this->select('field_config', 'fc')
      ->fields('fc')
      ->condition('id', $row->getSourceProperty('field_id'))
      ->execute()
      ->fetch();
    $field_definition['type']='entity_reference';
    $field_definition['module']='entity_reference';
    $row->setSourceProperty('field_definition', $field_definition);
    $reference_type = $row->getSourceProperty('type');
    $row->setSourceProperty('type','entity_reference');
    $row->setSourceProperty('reference_type',$reference_type);

    // Determine the translatable setting.
    $translatable = FALSE;
    return parent::prepareRow($row);
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'entity_type' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
      'bundle' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
      'field_name' => [
        'type' => 'string',
        'alias' => 'fci',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function count($refresh = FALSE) {
    return $this->initializeIterator()->count();
  }

  /**
   * {@inheritdoc}
   */
  public function defineValueProcessPipeline(MigrationInterface $migration, $field_name, $data) {
    $process = [
      'plugin' => 'sub_process',
      'source' => $field_name,
      'process' => [
        'target_id' => 'nid',
      ],
    ];
    $migration->setProcessOfProperty($field_name, $process);
  }

}
